import Button from './../components/HeaderButton'
import Background from './../components/HeaderBackground';

const theme = {
    background: '#2f353f',
    font: '#e4e4e4',
}

export default function Custom404() {
    return (
        <section className="hero is-fullheight has-text-centered">
            <div className="hero-body">
                <div className="container">
                    <h1 className='title'>404 - Page Not Found</h1>
                    <Button title="Return to site" icon="work" url="/" />
                </div>
                <Background color={theme.background}/>
            </div>
            <style jsx>{`
                .hero_body {
                    padding: 0
                }
                .title {
                    color: white;
                }
            `}</style>
        </section>
    )
}
