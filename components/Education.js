import React from 'react'
import Title from './Title'
import Item from './EducationItem'

export default class Education extends React.Component {
    render() {
        return (
            <section className="hero wrapper has-text-centered">
                <Title title="Education" color='#ffffff'/>
                <div className="container">
                    <Item
                        time="2021 - 2024 (M.Sc.)"
                        name="University of Technology"
                        major="Computer Science"
                        location="Delft"
                        color="#e67e22"
                    />
                    <Item 
                        time="2017 - 2021 (B.Sc.)"
                        name="University of Technology"
                        major="Computer Science"
                        location="Delft"
                        color="#2ecc71"
                    />
                    <Item 
                        time="2011 - 2017 (High School)"
                        name="Cygnus Gymnasium"
                        location="Amsterdam"
                        color="#3498db"
                    />
                </div>
                
                <style jsx>{`
                    .wrapper {
                        padding-top: 50px;
                        padding-bottom: 50px;
                        background: #c0392b url('/static/images/bg-edu.jpg');
                        background-size: cover;
                        color: #333333;
                        font-size: 1.3em;
                    }
                `}</style>
            </section>
        )
    }
}
