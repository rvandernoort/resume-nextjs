import React from 'react'
import { CgGames } from 'react-icons/cg'
import { BsCodeSlash, BsMusicNoteList, BsBook } from 'react-icons/bs'
import { GiGreenPower } from 'react-icons/gi'

export default class AboutInterest extends React.Component {
    render() {
        return (
            <div>
                <div className="columns topic">
                    <div className="column has-text-center is-6 is-offset-3 is-10-mobile is-offset-1-mobile">
                        My Interests
                    </div> 
                </div> 
                <div className="columns item">
                    <div className="column is-6 is-offset-3 is-8-mobile is-offset-2-mobile">
                        <div className="interest">
                            <BsCodeSlash size={70} />
                        </div>
                        <div className="interest">
                            <CgGames size={70} />
                        </div>
                        <div className="interest">
                            <BsMusicNoteList size={70} />
                        </div>
                        <div className="interest">
                            <BsBook size={70} />
                        </div>
                        <div className="interest">
                            <GiGreenPower size={70} />
                        </div>
                    </div>
                </div>
                <style jsx>{`
                    .topic {
                        font-weight: bold;
                        color: #ffcc00;
                        font-size: 1.2em;
                    }
                    .interest {
                        display: inline-block;
                        margin: 0 30px;
                        transition: all .2s ease-in-out;
                    }
                    .interest:hover {
                        transform: scale(1.5)
                    }
                `}</style>
            </div>
            
        )
    }
}
