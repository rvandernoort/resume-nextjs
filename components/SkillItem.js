import React from 'react'

export default class SkillItem extends React.Component {
    render() {
        let { name, img, color, value } = this.props
        return (
            <div className="column is-2-desktop has-text-center item" style={{'display': 'flex'}}>
                <div className="item_box">
                    <div className={'circle c100 p' + value + ' dark big orange'}>
                        <span>
                            <img src={img} alt=""/>
                        </span>
                        <div className="slice">
                            <div className="bar"></div>
                            <div className="fill"></div>
                        </div>   
                    </div>
                    <p className="name">{ name }</p>
                </div>
                <style jsx>{`
                    img {
                        margin-top: 30px;
                        max-width: 60px;
                        -webkit-transition: -webkit-transform 0.5s ease-in-out;
                        transition: transform 0.5s ease-in-out;
                    }
                    .item {
                        margin-bottom: 30px !important;
                    }
                    .item_box {
                        position: relative;
                    }
                    .name {
                        position: absolute;
                        top: 135px;
                        top: 100%;
                        left: 50%;
                        transform: translate(-50% , 0%);
                        -webkit-transform: translate(-50%, 0%);
                        color: #7f8c8d;
                    }
                    .circle:hover img {
                        -webkit-transform: rotate(-720deg);
                        transform: rotate(-720deg);
                    }
                    .bar {
                        border-color: ${color} !important;
                    }
                    .fill {
                        border-color: ${color} !important;
                    }
                    .circle {
                        margin: 10px auto;
                        -webkit-transition: -webkit-transform 0.5s ease-in-out;
                        transition: transform 0.5s ease-in-out;
                    }
                    .circle:hover {
                        -webkit-transform: rotate(360deg) scale(1.4);
                        transform: rotate(360deg) scale(1.4);
                        z-index: 999;
                    }
                `}</style>  
            </div>
        )
    }
}
