import React from 'react'
import Title from './Title'
import Group from './SkillItemGroup'
import circle from './../static/css/circle.css'

export default class Skills extends React.Component {
    render() {
        const skillImgs = [
            {
                name: 'DevOps',
                list: [
                    { name: 'git', color: '#F05032', value: '100' },
                    { name: 'scrum', color: '#1c9cb7', value: '100' },
                    { name: 'docker', color: '#2496ed', value: '100' },
                    { name: 'podman', color: '#8a629e', value: '80' },
                    { name: 'kubernetes', color: '#326DE5', value: '80' },
                    { name: 'argocd', color: '#ED7A4B', value: '80' },
                    { name: 'gha', color: '#4A7EBF', value: '90' },
                    { name: 'npm', color: '#cb3736', value: '100' },
                    { name: 'linkerd', color: '#69C1AF', value: '20' },
                ]
            },
            { 
                name: 'Programming Languages',
                list: [
                    { name: 'javascript', color: '#f7df1e', value: '90' },
                    { name: 'typescript', color: '#0078cf', value: '80' },
                    { name: 'php', color: '#8993be', value: '50' },
                    { name: 'java', color: '#1e77b7', value: '50' },
                    { name: 'scala', color: '#ff311c', value: '60' },
                    { name: 'kotlin', color: '#7b71e4', value: '40' },
                    { name: 'python', color: '#3271a0', value: '100' },
                    { name: 'c++', color: '#659bd3', value: '30' },
                    { name: 'agda', color: '#0e0e0e', value: '40' },
                    { name: 'prolog', color: '#ed1c24', value: '40' },
                    { name: 'golang', color: '#4DD0E1', value: '60' },
                ]
            },
            {
                name: 'AI Frameworks',
                list: [
                    { name: 'pytorch', color: '#ee4b28', value: '80' },
                    { name: 'sklearn', color: '#f7931e', value: '60' },
                    { name: 'transformers', color: '#ffd21e', value: '50' },
                    { name: 'llama.cpp', color: '#a1511c', value: '50' },
                    { name: 'ollama', color: '#2F353F', value: '50' },
                ]
            },
            {
                name: 'Message Brokers',
                list: [
                    { name: 'rabbitmq', color: '#FF6600', value: '40' },
                    { name: 'redis', color: '#C63E2B', value: '20' },
                    { name: 'mqtt', color: '#660066', value: '60' },
                    { name: 'pubsub', color: '#4486F9', value: '50' },
                ],
            },
            {
                name: 'Back-ends',
                list: [
                    { name: 'django', color: '#003e2b', value: '80' },
                    { name: 'flask', color: '#0e0e0e', value: '60' },
                    { name: 'nodejs', color: '#81ca2a', value: '50' },
                    { name: 'ros', color: '#1b2a49', value: '50' },
                    { name: 'grafana', color: '#f69119', value: '50' },
                ]
            },
            {
                name: 'Databases',
                list: [
                    { name: 'mysql', color: '#0784af', value: '80' },
                    { name: 'postgres', color: '#2f6792', value: '40' },
                    { name: 'sqlite', color: '#1a8cd2', value: '60' },
                    { name: 'influxdb', color: '#4BABED', value: '50' },
                    { name: 'mongodb', color: '#5C9B45', value: '50' },
                ]
            },
            {
                name: 'Front-end',
                list: [
                    { name: 'html5', color: '#f16529', value: '100' },
                    { name: 'css3', color: '#29a9df', value: '80' },
                    { name: 'sass', color: '#cb6699', value: '35' },
                    { name: 'bootstrap', color: '#8b57d9', value: '80' },
                    { name: 'jquery', color: '#288cc4', value: '35' },
                    { name: 'nextjs', color: '#ffffff', value: '40' },
                    { name: 'reactjs', color: '#61d9fa', value: '50' },
                    { name: 'vuejs', color: '#41b883', value: '50' },
                    { name: 'wordpress', color: '#1b769c', value: '45' },
                ]
            },
        ]

        return (
            <section className="hero wrapper has-text-centered">
                <style dangerouslySetInnerHTML={{ __html: circle }} />
                <Title title="Skills" color='#ffffff'/>
                <div className="container">
                    <div className="columns">
                        <div className="column detail is-10 is-offset-1 is-6-desktop is-offset-3-desktop is-10-mobile is-offset-1-mobile">
                            Here I list the <b>technologies and skills</b> that I have worked with so far. <br/>
                            However, I'm always intested in learning <i>new</i> things!
                        </div>
                    </div>
                    {
                        skillImgs.map((item, key) => (
                            <Group key={key} name={item.name} items={item.list.sort((a, b) => (parseInt(a.value) > parseInt(b.value)) ? 1 : -1).reverse()} />    
                        ))
                    }
                </div>
                
                <style jsx>{`
                    .wrapper {
                        padding-top: 50px;
                        padding-bottom: 50px;
                        background: #2f353f;
                        background-size: cover;
                        color: #ffffff;
                        font-size: 1.3em;
                    }
                    .detail {
                        font-size: 0.95em;
                    }
                    .detail > b {
                        color: #F1A9A0;
                    }
                    .detail > i {
                        color: #00E640;
                    }
                `}</style>
            </section>
        )
    }
}
