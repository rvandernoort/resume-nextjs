import React from 'react'
import Title from './Title'
import Timeline from './ExperiencesTimelineItem'
import { BsLinkedin } from 'react-icons/bs'


export default class Experiences extends React.Component {
    render() {
        return (
            <section className="hero wrapper has-text-centered">
                <Title title="Experiences" color='#2c3e50' id="experiences"/>
                <div className="container">
                    <Timeline side="left" time="Sep 2017 - Feb 2021" color="#2ecc71">
                        <h4 style={{ 'color': '#2ecc71' }}>BSc Computer Science and Engineering</h4>
                    </Timeline>
                    <Timeline side="right" time="Sep 2019 - Feb 2020" color="#2c3e50">
                        <h4 style={{ 'color': '#2c3e50' }}>Aves Robotics</h4>
                        <p>
                            <i className="position">Minor Robotics</i><br/>
                            Proof of concept of a <b>luggage stacking robot</b> for the cargo hull of a plane. <br/>
                            <a href="https://robohouse.nl/madein/minor-robotics-2019-2020/">Wingspan</a> by team Aves Robotics.
                            Comissioned by <b>KLM Apron Services</b> and <b>Vanderlande</b>.
                        </p>
                    </Timeline>
                    <Timeline side="left" time="Nov 2020 - Feb 2021" color="#e67e22">
                        <h4 style={{'color': '#e67e22'}}>TOPDesk</h4>
                        <p>
                            <i className="position">Bachelor End Project</i><br/>
                            <b>Automate</b> Maturatiy Analysis process<br/>
                            Full-stack development project (<a href="http://resolver.tudelft.nl/uuid:8c834d3f-afc7-4dd1-aebf-5573e1a7a8e7">paper</a>)
                        </p>
                    </Timeline> 
                    <Timeline side="right" time="Mar 2020 - current" color="#3498db">
                        <h4 style={{'color': '#3498db'}}>MSC Computer Science</h4>
                        <p>
                            <i className="position">Specialisation Software Engineering</i><br/>
                        </p>
                    </Timeline>
                    <Timeline side="left" time="Sep - Nov 2022" color="#e74c3c">
                        <h4 style={{'color': '#e74c3c'}}>TypeSpaceBERT</h4>
                        <p>
                            <i className="position">Machine Learning for Software Engineering Project</i><br/>
                            Energy Aware Extension for PyTorch. (<a href="https://github.com/ML4SE2022/group5">GitHub</a>)
                        </p>
                    </Timeline>
                    <Timeline side="right" time="Feb - Apr 2023" color="#2ecc71">
                        <h4 style={{'color': '#2ecc71'}}>GATorch</h4>
                        <p>
                            <i className="position">Sustainable Software Engineering Project</i><br/>
                            Energy Aware Extension for PyTorch. (<a href="https://github.com/GreenAITorch/GATorch">GitHub</a>)
                        </p>
                    </Timeline>
                    <Timeline side="left" time="Sep 2023 - Apr 2024" color="#e67e22">
                        <h4 style={{'color': '#e67e22'}}>MSc Thesis</h4>
                        <p>
                            <i className="position">Scalability of Edge AI</i><br/>
                            Thesis project under supervision of <a href="https://luiscruz.github.io">Luis Cruz</a> on <b>Green AI.</b><br/>Sustainability of Edge AI at scale (<a href="https://repository.tudelft.nl/islandora/object/uuid%3A78ac2503-426c-406b-a88f-8c0ed1abe0c0">Thesis</a>)
                        </p>
                    </Timeline>
                    <Timeline side="right" time="Jun 2024 - Now" color="#6e00b3">
                        <h4 style={{'color': '#e67e22'}}>HomeWizard</h4>
                        <div style={{'display': 'flex', 'justifyContent': 'center', 'alignItems': 'center', 'flexDirection': 'row'}}>
                            <a href="https://homewizard.com" style={{'paddingRight': '20px', 'paddingTop': '10px'}}>
                                <img src="/static/images/hw-logo.png" alt="test" width="150px" height="150px"/>
                            </a>
                            <p>
                                <i className="position">Cloud Engineer</i><br/>
                                Currently working at <a href="https://www.homewizard.com/">HomeWizard</a> as a Cloud Engineer, responsible for the development of the cloud infrastructure with millions of active devices.
                            </p>
                        </div>
                    </Timeline>
                    <Timeline side="left" time="Future" color="#3498db">
                        <h4 style={{'color': '#3498db', 'paddingBottom': '10px'}}>Future endeavors</h4>
                        <div style={{'display': 'flex', 'justifyContent': 'center', 'alignItems': 'center', 'flexDirection': 'row'}}>
                            <p>
                                Always interested in new sustainable projects.
                                Feel free to contact me if you want to have a chat! <a style={{ cursor: "pointer" }} className="linkedin" href="https://www.linkedin.com/in/rover-van-der-noort-35bb64190/">Linked
                                    <BsLinkedin />
                                </a><br/>
                                Or support my work by <a href="https://www.buymeacoffee.com/rovervandernoort">buying me a coffee!</a>
                            </p>
                            <a href="https://www.buymeacoffee.com/rovervandernoort" style={{'paddingLeft': '20px', 'paddingTop': '10px'}}>
                                <img src="/static/images/bmc_qr.png" alt="test" width="150px" height="150px"/>
                            </a>
                        </div>
                    </Timeline>
                </div>
                
                <style jsx>{`
                    .wrapper {
                        padding-top: 50px;
                        padding-bottom: 50px;
                        background: #bdc3c7;
                        color: #333333;
                        font-size: 1.3em;
                    }
                    h4 {
                        font-size: 1.2em;
                        font-weight: bold;
                    }
                    p {
                        font-size: 0.8em;
                    }
                    i.position {
                        color: #c0392b;
                    }
                `}</style>
            </section>
        )
    }
}
