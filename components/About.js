import React from 'react'
import Title from './Title'
import Interersts from './AboutInterest'
import { BsLinkedin } from 'react-icons/bs'

export default class About extends React.Component {
    render() {
        return (
            <section className="hero wrapper has-text-centered">
                <Title title="About me" color='#ffffff'/>
                <div className="columns is-mobile">
                    <div className="column has-text-right-tablet has-text-center is-12-mobile is-6-desktop">
                        <dl>
                            <dt>FULL NAME</dt>
                            <dd>Ir. Rover van der Noort MSc</dd>
                            <dt>Institution</dt>
                            <dd>University of Technology Delft</dd>
                            <dt>
                                <a style={{ color: "#ffcc00", cursor: "pointer" }} className="linkedin" href="https://www.linkedin.com/in/rover-van-der-noort-35bb64190/">Linked
                                    <BsLinkedin />
                                </a>
                            </dt>
                        </dl>
                    </div>
                    <div className="column has-text-left">
                        <img src="/static/images/mascot.png" className="mascot" />
                    </div>
                </div>
                <div className="columns">
                    <div className="column detail is-10 is-offset-1 is-6-desktop is-offset-3-desktop is-10-mobile is-offset-1-mobile">
                        Hi there, my name is <b>Rover</b>.
                        I'm a master graduate in Software Engineering at the <b>University of Technology Delft</b>, the Netherlands.
                        I like working on large <i>software architecture</i> problems. I also have an interest in <i>programming languages</i>.
                        Moreover, I love learning new technologies and to contribute to <b>sustainable</b> solutions!
                        If you want to contact me, you can reach me on <a href="https://www.linkedin.com/in/rover-van-der-noort-35bb64190/">LinkedIn</a>.
                    </div>
                </div>
                <Interersts />
                <style jsx>{`
                    .wrapper {
                        padding-top: 50px;
                        padding-bottom: 50px;
                        background: #34495e;
                        color: #ffffff;
                        font-size: 1.3em;
                    }
                    dl > dt {
                        color: #ffcc00;
                        font-weight: bold;
                    }
                    dl > dd {
                        margin-bottom: 30px;
                    }
                    .mascot {
                        margin-left: 20px;
                        width: 180px;
                    }
                    .detail {
                        font-size: 0.95em;
                    }
                    .detail > b {
                        color: #F1A9A0;
                    }
                    .detail > i {
                        color: #00E640;
                    }
                    .linkedin:hover {
                        color: #363636 !important;
                    }
                `}</style>
            </section>
        )
    }
}
