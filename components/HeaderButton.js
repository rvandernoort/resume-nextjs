import React from 'react'
import { BsGithub } from 'react-icons/bs'
import { FaFileDownload } from "react-icons/fa";
import { SiGitlab } from 'react-icons/si'
import { IoCodeWorking } from 'react-icons/io5'

export default class HeaderButton extends React.Component {
    render() {
        let {title, icon, url, fileName=null} = this.props
        return (
            <a className="button is-primary is-medium" href={url} download={fileName}>
                { icon === "gh" ? <BsGithub /> : 
                    icon === "gl" ? <SiGitlab /> :
                    icon === "work" ? <IoCodeWorking /> : 
                    icon == "download" ? <FaFileDownload /> : ""
                }
                <span>{title}</span>
                <style jsx>{`
                    a {
                        margin: 0 5px;
                        padding: 0 20px;
                        border-radius: 360px;
                    }
                    span {
                        margin-left: 5px;
                    }
                `}</style>
            </a>
        )
    }
}
