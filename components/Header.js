import React from 'react'
import Background from './HeaderBackground';
import ProfileImage from './HeaderProfile';
import Brace from './HeaderBrace'
import Title from './HeaderTitle'
import Button from './HeaderButton'
const themes = {
    light: {
        background: '#f5f5f5',
        font: '#4a4a4a'
    },
    dark: {
        background: '#2f353f',
        font: '#e4e4e4'
    }
}

export default class Header extends React.Component {
    constructor() {
        super()
        this.state = {
            theme: 'dark'
        }
    }

    toggleTheme() {
        let newtheme = this.state.theme === 'light' ? 'dark' : 'light'
        this.setState({
            theme: newtheme
        })
    }

    render() {
        let theme = themes[this.state.theme];
        return (
            <section className="hero is-fullheight has-text-centered">
                <div className="hero-body">
                    <div className="container">
                        <div className="columns is-mobile">
                            <Brace type="left" color={theme.font}/>
                            <ProfileImage toggle={this.toggleTheme.bind(this)} />
                            <Brace type="right" color={theme.font}/>
                        </div>
                        <div className="columns">
                            <div className="column">
                                <Title color={theme.font}/>
                            </div>
                        </div>
                        <div className="columns">
                            <div className="column">
                                <Button title="My GitHub" icon="gh" url="https://github.com/rvandernoort" />
                                <Button title="My Gitlab" icon="gl" url="https://gitlab.com/rvandernoort" />
                                <Button title="My Work" icon="work" url="#experiences" />
                                <Button title="Resume (pdf)" icon="download" url="/static/pdf/resume_rover.pdf" fileName="resume_rover.pdf" />
                                <Button title="Portfolio (pdf)" icon="download" url="/static/pdf/portfolio_rover.pdf" fileName="portfolio_rover.pdf" />                             
                            </div>
                        </div>
                    </div>
                    <Background color={theme.background}/>
                </div>
                <style jsx>{`
                    .hero_body {
                        padding: 0
                    }
                `}</style>
            </section>
        )
    }
}
