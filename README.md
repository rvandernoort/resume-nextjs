# Resume with NextJS

Resume website with NextJS

## Compilation
**on development**
```bash
yarn dev
```
**on production**
```bash
yarn build
```

## Authors

* **Rover van der Noort** - [https://gitlab.com/rvandernoort](https://gitlab.com/rvandernoort)
* **NutAnek** -  [https://github.com/nutanek](https://github.com/nutanek)
